<h3 align="center">ChatBot - Adico</h3>

---

<p align="center"> Sans rentrer dans une logique de déploiement d’un robot pour remplacer un être humain, un chatbot peut devenir un réel gain de temps pour vous, élus et agents, en collectivités. Dès lors qu’un administré souhaite obtenir une réponse rapide à une question simple, il peut passer par ce canal de discussion. Vous verrez qu'avec notre accompagnement, la mise en place de ce chatbot se révèlera être un jeu d'enfant !
    <br> 
</p>

## 🏁 Installation

### Prérequis
<ul>
<li>Serveur Web : Apache, Nginx, ou autre.</li>
<li>PHP : Version 7.x ou supérieure.</li>
<li>Système de Gestion de Base de Données (SGBD) : MySQL, PostgreSQL, SQLite, etc.</li>
<li>Extensions PHP : pdo_mysql, mysqli, ou autre, selon le SGBD.</li>
<li>Accès aux droits de la base de données : Créez un utilisateur avec les autorisations nécessaires.</li>
</ul>

### Installation

Cloner le répertoire dans le dossier souhaité. 

```
git clone <url du dépot>
```

Modifier le fichier Config.php à la racine du dossier 
```
// Information sur la BDD
Config::write('db.host', 'SERVEUR BDD');
Config::write('db.port', '3306');
Config::write('db.basename', 'NOM DE LA BDD');

// Information sur l'utilisateur de la BDD
Config::write('db.user', 'USER DE LA BDD');
Config::write('db.password', 'MDP DE LA BDD');

//token
Config::write('token.key', 'code_a_changer');
Config::write('token.validite', '14400');

// Ici mettre l'url de votre chatbot incluant les https://
Config::write('domaine', 'https://monadresse.com/');
```

Modifier dans le dossier "cadreChatbot" puis chatbot.js modifier la ligne 6
```
// Nom de domaine
var domaine = '';
```

Importer la BDD via le SQL fourni dans le dossier new nommer chatbot.sql

## 👨‍💻 Première connexion

Accéder à votre url puis connecter vous avec les identifiants suivants. 
```
Login : demo@demo.fr
Mot de passe : DemoChatbot!123
```

## Déploiement sur un site
```
<script type="text/javascript"
src="nom_du_domaine/chatbot/cadreChatbot/chatbot.js?botId=id_du_bot_correspondant" 
id="flex_chatbot-js"></script>
```